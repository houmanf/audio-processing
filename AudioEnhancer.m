classdef AudioEnhancer
    properties 
        mAudios  % array of Audio
        mAudioUtils
    end
    methods
        function obj = AudioEnhancer(audios, audioUtils)
            obj.mAudios = audios;
            obj.mAudioUtils = audioUtils;
        end
        
        % return the overlapping range across all audios
        function [startIndex, endIndex] = getOverlappingRange(obj, overlaps)
            startIndex = overlaps(1,1);
            endIndex = overlaps(1,2);
            for i=2:numel(obj.mAudios)
                startIndex = max(startIndex, overlaps(i,1));
                endIndex = min(endIndex, overlaps(i,2));
            end
        end
        
        function combinedAudio = combine(obj, overlaps, filePath)              
            % find the overlapping range between all audios
            [startIndex, endIndex] = obj.getOverlappingRange(overlaps);

            audioCount = numel(obj.mAudios);
                 
            range = endIndex - startIndex + 1;
            range = 8000*10;
   
            combinedSamples = zeros(range,1);
            amplifyRatio = 1;
            
            higherSamplesPercentile = zeros(audioCount,1);
            lowerSamplesPercentile = zeros(audioCount,1);
            for i=1:audioCount
                lowerSamplesPercentile(i) = prctile(abs(obj.mAudios(i).getSamples()),10);
                higherSamplesPercentile(i) = prctile(abs(obj.mAudios(i).getSamples()),90);
            end
            
            for i=1:range                
                accumulation = [];
%                 areAllSignalsAboveNoiseThreshohld = true;
%                 for j=1:audioCount                    
%                     if (obj.mAudios(j).getSample(overlaps(j,1)+i) < lowerSamplesPercentile(j))
%                         areAllSignalsAboveNoiseThreshohld = false;
%                     end
%                 end
                for j=1:audioCount
                    signal = obj.mAudios(j).getSample(overlaps(j,1)+i);
                    if (abs(signal) <= higherSamplesPercentile(j) && abs(signal) >= lowerSamplesPercentile(j))
                        ampedSignal = signal * amplifyRatio;
                        accumulation = [accumulation ampedSignal];
                    else
                        ampedSignal = signal;
                        accumulation = [accumulation ampedSignal];
                    end
                end
%                 if (areAllSignalsAboveNoiseThreshohld)
                
                combinedSamples(i) = mean(accumulation);
%                 else
%                     combinedSamples(i) = mean(accumulation) * amplifyRatio;
%                 end
            end
            
            freq = obj.mAudios(1).getFreq();
            combinedAudio = Audio();
            combinedAudio.setFreq(freq);
            combinedAudio.setSamples(combinedSamples);
            combinedAudio.saveAudio(1,range,filePath);
        end
       
    end
end