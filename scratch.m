 store audio files in the .mat fromat. 

path = cellstr([
                '/home/houman/Desktop/data/1/1.wav';
                '/home/houman/Desktop/data/2/2.wav';
                '/home/houman/Desktop/data/3/3.wav';
                '/home/houman/Desktop/data/4/4.wav';
                '/home/houman/Desktop/data/5/5.wav';]);

for i = 1 : numel(path)
    fprintf ('Printing file %i ... \n', i);
    Audio.saveData(path{i});
end

%% read files

path = cellstr(['/home/houman/workspace/matlab/audio enhancement/data/Programming(5)/1/data.mat';
                '/home/houman/workspace/matlab/audio enhancement/data/Programming(5)/3/data.mat';
                '/home/houman/workspace/matlab/audio enhancement/data/Programming(5)/4/data.mat']);

for i = 1 : numel(path)
    fprintf ('Reading file %i ... \n', i);
    temp = load(path{i});
    audio(i) = temp;
end


%% plot signals

figure
samples = audio.y;
plot((0:numel(samples)-1)/audio.Fs, samples);

%%

mins = 10;
sec = mins*60;
cut = sec*audio(3).f;

[n,d] = rat(audio(3).f/audio(2).f);
audio(2).S = resample(audio(2).S,n,d);

[C, Lags] = xcorr(audio(2).S(1:cut), audio(3).S(1:cut));

[~,I] = max(abs(C));
timeDiff = Lags(I)/audio(3).f;

s3 = audio(3).S(round(abs(timeDiff)*16000):end);
s2 = audio(2).S(1:end);

audiowrite('/home/houman/Desktop/s2.wav', s2, 16000);
audiowrite('/home/houman/Desktop/s3.wav', s3, 16000);

for i=1:min(numel(s2),numel(s3))
    a(i) = (s3(i)+s2(i))/2;
end

audiowrite('/home/houman/Desktop/a.wav', a, 16000);


%%

a = [1,2,3,4,5,6,7,8,9,10];
b = [6,7,8,9,10,1,2,3,4,5];

[C, Lags] = xcorr(a, b, 'unbiased');

for i = 1:numel(C)
    fprintf ('%i : %i \n\n', round(C(i)), round(Lags(i)));
end

%%

a1 = Audio('/home/houman/Desktop/data/1/data.mat');
a2 = Audio('/home/houman/Desktop/data/2/data.mat');
a3 = Audio('/home/houman/Desktop/data/3/data.mat');
a4 = Audio('/home/houman/Desktop/data/4/data.mat');
a5 = Audio('/home/houman/Desktop/data/5/data.mat');

A = [a1; a2; a3; a4; a5];
Audio.combine(A,1200);


%%

a = Audio('/home/houman/Desktop/folder/audio.mp3');

a.splitAndSaveBySliceDuration(10000);


%%

paths = {

%                   '/home/houman/workspace/matlab/audio enhancement/data/Programming/processed/1/audio.wav';
%                   '/home/houman/workspace/matlab/audio enhancement/data/Programming/processed/2/audio.wav';
%                   '/home/houman/workspace/matlab/audio enhancement/data/Programming/processed/3/audio.wav';
%                   '/home/houman/workspace/matlab/audio enhancement/data/Programming/processed/4/audio.wav';
%                   '/home/houman/workspace/matlab/audio enhancement/data/Programming/processed/5/audio.wav';

            '/home/houman/workspace/matlab/audio enhancement/data/perf/processed/1.flac';
            '/home/houman/workspace/matlab/audio enhancement/data/perf/processed/2.flac';
            '/home/houman/workspace/matlab/audio enhancement/data/perf/processed/3.flac';
            '/home/houman/workspace/matlab/audio enhancement/data/perf/processed/4.flac';                          
            '/home/houman/workspace/matlab/audio enhancement/data/perf/processed/5.flac';                   
            '/home/houman/workspace/matlab/audio enhancement/data/perf/processed/6.flac';    
                                         

          };

audios = [];
for i = 1: size(paths,1)
    a = Audio();
    a.loadAudio(paths{i});
    audios = [audios a];
end
 
%%

audioUtils = AudioUtils(audios);

minFreq = audioUtils.syncFreq();
overlaps = audioUtils.getAudiosOverlapAsSampleIndex(60);

%% combine audios

audioEnhancer = AudioEnhancer(audios, audioUtils);
audioEnhancer.combine(overlaps,'/home/houman/Desktop/perf-cync-amped.wav');

%%

% for i = 1:numel(audios)
%     audios(i).trim(overlaps(i,1),overlaps(i,2));
%     path = sprintf('/home/houman/Desktop/output/%i', i);
%     audios(i).splitAndSaveBySliceDuration(14000, path);
%     
%     silenceThreshold = prctile(abs(audios(i).getSamples()),35);
%     [splitFreq, splitArray] = audios(i).splitAndSaveAtSilence(5000, 15000, 250, silenceThreshold, path);
% end

%%

path = '/home/houman/Desktop/perf-output-8000Hz/1';
silenceThreshold = prctile(abs(audios(1).getSamples()),40);
[splitFreq, splitArray] = audios(1).splitAndSaveAtSilence(5000, 12000, 250, silenceThreshold, path);

%%

for i = 2:numel(audios)
    freq = audios(i).getFreq();
    freqAdjustmentRatio = freq/splitFreq;
    adjustedSplitArray = int64(splitArray * freqAdjustmentRatio);
    
%     adjustedOverlaps = int64(overlaps * freqAdjustmentRatio);
%     audios(i).trim(adjustedOverlaps(i,1),adjustedOverlaps(i,2));
    
    path = sprintf('/home/houman/Desktop/perf-output-8000Hz/%i', i);
    audios(i).splitAndSaveBySplitArray(adjustedSplitArray, path);
end

%% trim
% 
% for i = 1:numel(audios)
%     freq = audios(i).getFreq();
%     freqAdjustmentRatio = freq/minFreq;
%     adjustedOverlaps = int64(overlaps * freqAdjustmentRatio);
%     audios(i).trim(adjustedOverlaps(i,1),adjustedOverlaps(i,2));
% end

%% 




























































































