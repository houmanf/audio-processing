classdef Audio < handle
    % A class for an audio file
    
    properties (SetAccess = private, GetAccess = public)
        mSamples               % array of samples
        mSamplesCount       % number of samples
        mFreq               % sampling rate
        mFilePath        % path of the original file (file type examples: .mat, .mp3)
        mReady = 0;             % is the audio loaded and ready to use
    end
    
    methods
        % constructor
        function obj = Audio()
        end
        
        % load an audio file which is stored in .mat format
        function loadAudio(obj, filePath)
            [~, ~, ext] = fileparts(filePath);
            if strcmp(ext, '.mat')
                tmp = load(filePath);
                obj.mSamples = tmp.S;
                obj.mFreq = tmp.f;
                clear tmp;
            else
                [S, f] = audioread(filePath);
                S = S(:,1); % only samples from the first channel
                obj.mSamples = S;
                obj.mFreq = f;           
            end            
            obj.mFilePath = filePath;
            
            obj.mSamplesCount = numel(obj.mSamples);
            obj.mReady = 1;
        end
        
        % save audio as a .mat file
        % from: index of the first sample to be written in file
        % to: index of the last sample to be written in file
        function saveData (obj, from, to, filePath)
            if nargin == 2 

                [partialPath, fileName, ~] = fileparts(obj.mFilePath);
                filePath = sprintf('%s/%s.%s', partialPath, fileName, 'mat');
            end
                        
            S = obj.getSamples(from, to);
            f = obj.getFreq();
            
            save(filePath, '-v7.3', 'S', 'f');
            
            fprintf('Data stored at: %s \n', filePath);
        end
        
        function val = getFreq(obj)
            val = obj.mFreq;
        end
        
        function setFreq(obj,freq)
            obj.mFreq = freq;
        end
        
        % save this audio as an audio file (.wav, .flac, ...)
        function saveAudio(obj, from, to, filePath)
            if nargin == 2
                from = 1;
                to = numel(obj.mSamples);
            end
            audiowrite(filePath, obj.getSamples(from,to), obj.mFreq);            
        end

        function filePath = getFilePath(obj)
            filePath = obj.mFilePath;
        end
        
        function samples = getSamples(obj, from, to)
            if nargin == 1
                from = 1;
                to = numel(obj.mSamples);
            end
            samples = obj.mSamples(from:to);
        end
        
        function sample = getSample(obj, index)
            sample = obj.getSamples(index, index);
        end
        
        function obj = setSamples(obj, samples)
            obj.mSamplesCount = numel(samples); 
            obj.mSamples = samples;
        end
        
        % this function resamples to update the frequency
        function obj = resample(obj, freq)
            [n,d] = rat(freq/obj.mFreq);
            obj.mSamples = resample(obj.mSamples,n,d);
            obj.mSamplesCount = numel(obj.mSamples);
            obj.mFreq = freq;
        end
        
        % split and save samples and save
        % sliceNum: number of pieces to break audio into
        function splitAndSaveBySliceNum(obj, sliceNum, baseDirPath)
            sliceSampleCount = floor(size(obj.mSamples)/sliceNum);
            if nargin == 2
                obj.splitAndSaveBySliceDuration(sliceSampleCount);
            else
                obj.splitAndSaveBySliceDuration(sliceSampleCount, baseDirPath); 
            end
        end
        
        % sliceDuration: ms
        function splitAndSaveBySliceDuration(obj, sliceDuration, baseDirPath)
            
            sliceSamplesCount = sliceDuration * obj.mFreq * power(10,-3);
                   
            if nargin == 2
                [pathStr, ~, ~] = fileparts(obj.mFilePath);
                baseDirPath = pathStr;
            end
            
            dirPath = baseDirPath;
            mkdir(dirPath);
            disp(['Created output directory at: ' dirPath]);
            
            c = 1;
            to = sliceSamplesCount;
            samplesCount = size(obj.mSamples,1);
            
            while to + 1 < samplesCount;
                from = sliceSamplesCount*(c-1)+1;
                if to+sliceSamplesCount <= samplesCount 
                    to = sliceSamplesCount*c;
                else
                    to = samplesCount;
                end
                filePath =  sprintf('%s/%i.%s', dirPath, c, 'flac'); 
                disp(['Writing file: ' filePath]);
                obj.saveAudio(from, to, filePath);
                c = c+1;
            end
        end
        
        % minDuration: the minimum duration of each slice
        % baseDirPath: the path to where the output is saved
        % silenceSplitDuration: the duration of the spliting silence (ms)
        % silenceThreshold: the threshold for a signal to be silent
        function [freq, splitIndexArray] = splitAndSaveAtSilence(obj, minimumDuration, maximumDuration, silenceSplitDuration, silenceThreshold, dirPath)
             
            minBlockSamplesCount = minimumDuration * obj.mFreq * power(10,-3);
            minSilenceBlockSamplesCount = silenceSplitDuration * obj.mFreq * power(10,-3);
            maximumBlockSamplesCount = maximumDuration * obj.mFreq * power(10,-3);
                   
            % if dirPath is not passed in use the dir path of the audio file
            if nargin == 2
                [pathStr, ~, ~] = fileparts(obj.mFilePath);
                dirPath = pathStr;
            end

            mkdir(dirPath);
            disp(['Created output directory at: ' dirPath]);
            
            from = 1;
            to = minBlockSamplesCount;
            
            samplesCount = size(obj.mSamples,1);
           
            count = 1;
            splitIndexArray = [];
            while to + 1 < samplesCount;
                silenceFound = false;                
                to = to + minBlockSamplesCount;

                while (~silenceFound)
                    % jump 10 samples ahead
                    to = to + 10;
                    silenceBlockStartIndex = to;
                    silenceBlockEndIndex = silenceBlockStartIndex + minSilenceBlockSamplesCount;
                    if silenceBlockEndIndex < samplesCount                         
                        silenceBlockAverage = mean(abs(obj.mSamples(silenceBlockStartIndex:silenceBlockEndIndex)));                        
                        if (silenceBlockAverage <= silenceThreshold || to-from > maximumBlockSamplesCount)
                            to = silenceBlockEndIndex;
                            silenceFound = true;
                            if (to-from > maximumBlockSamplesCount)
                                fprintf('Cut the audio at %ims as silence was not found.\n', maximumDuration);
                            end
                        end
                    else
                        silenceFound = true;
                        to = samplesCount;
                    end
                end
                
                splitIndexArray(count) = to;
                filePath =  sprintf('%s/%i.%s', dirPath, count, 'flac'); 
                disp(['Writing file: ' filePath]);
                obj.saveAudio(from, to, filePath);
                from = to;
                count = count + 1;
            end
            freq = obj.mFreq;
        end
        
        % splitArray: An array containing samples indexes as splitting
        % points
        function splitAndSaveBySplitArray(obj, splitArray, dirPath)
            
            % if dirPath is not passed in use the dir path of the audio file
            if nargin == 2
                [pathStr, ~, ~] = fileparts(obj.mFilePath);
                dirPath = pathStr;
            end

            mkdir(dirPath);
            disp(['Created output directory at: ' dirPath]);
            
            samplesCount = size(obj.mSamples,1);
            from = 1;
           
            index = 1;
            samplesLeft = true;
            while index <= length(splitArray) && samplesLeft
                to = splitArray(index);
                if (to > samplesCount)
                    to = samplesCount;
                    samplesLeft = false;
                end
                
                filePath =  sprintf('%s/%i.%s', dirPath, index, 'flac'); 
                disp(['Writing file: ' filePath]);
                obj.saveAudio(from, to, filePath);
                
                from = to;
                index = index + 1;
            end
          
        end
        
        function trim(obj, from, to)
            if (to > obj.mSamplesCount)
                to = obj.mSamplesCount;
            end
            obj.mSamples = obj.mSamples(from:to);
            obj.mSamplesCount = obj.getSamplesCount();
        end
        
        function count = getSamplesCount(obj)
            count = obj.mSamplesCount;
        end
        
        function samplesAverage = getSamplesAverage(obj)
            samplesAverage = mean(abs(obj.mSamples));
        end
    end  
end

