classdef AudioUtils
    
    properties
        mAudios  % array of Audio
        mAudioEnhancer
        mCount = 0;   % number of Audio objects in audios
        mMaxFreq % min frequency among all Audio objects in mAudios
        mMinFreq
    end
    
    methods
        function obj = AudioUtils(audios)
            obj.mAudios = audios;
            obj.mCount = numel(obj.mAudios);
            obj.mMaxFreq = AudioUtils.calculateMaxFreq(audios);
            obj.mMinFreq = AudioUtils.calculateMinFreq(audios);
        end
          
        function minFreq = syncFreq(obj)
            minFreq  = 8000;
%             minFreq = obj.getMinFreq();
            
            for i = 1:obj.mCount
                if obj.mAudios(i).getFreq() ~= minFreq
                    obj.mAudios(i) = obj.mAudios(i).resample(minFreq);
                end
            end
        end
        
        function count = getCount(obj)
            count = obj.mCount;
        end
        
        function overlap = getAudiosOverlapAsSampleIndex(obj, sec)
            
            % how many samples at the beginning/end of each Audio should be used for time synchronization
            numSamplesForAnalysis = sec*obj.mMinFreq;
           
            overlap = zeros(obj.mCount,2);
            
            for i = 1:obj.mCount
                overlap(i,1) = 1;
                overlap(i,2) = obj.mAudios(i).getSamplesCount();
            end
                                  
            for i = 1:obj.mCount-1                
                for j = i+1:obj.mCount
                    
                    % two references of Audios
                    A = obj.mAudios(i);
                    B = obj.mAudios(j);
                    
                    beginningSamplesA = A.getSamples(1,numSamplesForAnalysis);
                    beginningSamplesB = B.getSamples(1,numSamplesForAnalysis);
                    [beginningC, beginningLags] = xcorr(beginningSamplesA, beginningSamplesB);         
                                       
                    endSamplesA = A.getSamples(A.getSamplesCount()-numSamplesForAnalysis, A.getSamplesCount());
                    endSamplesB = B.getSamples(B.getSamplesCount()-numSamplesForAnalysis, B.getSamplesCount());
                    [endC, endLags] = xcorr(endSamplesA, endSamplesB);

                    [~,beginningI] = max(abs(beginningC));
                    [~,endI] = max(abs(endC));
                                       
                    if beginningLags(beginningI) < 0
                        if abs(beginningLags(beginningI)) > overlap(i,1)
                            overlap(j,1) = abs(beginningLags(beginningI));
                        end
                    end
                    if beginningLags(beginningI) > 0
                        if beginningLags(beginningI) > overlap(j,1)
                            overlap(i,1) = beginningLags(beginningI);
                        end
                    end
                    
                    if endLags(endI) > 0
                        newEndIndex = B.getSamplesCount() - abs(endLags(endI));
                        if newEndIndex < overlap(j,2)
                            overlap(j,2) = newEndIndex;
                        end
                    end
                    if endLags(endI) < 0
                        newEndIndex = A.getSamplesCount() - abs(endLags(endI));
                        if newEndIndex < overlap(i,2)
                            overlap(i,2) = newEndIndex;
                        end
                    end
                end
            end
                        
            for i= 1:obj.mCount
                fprintf('Audio %i is %0.3f/%0.3f samples ahead or behined at the beginning/end.\n', i, overlap(i,1), obj.mAudios(i).getSamplesCount()-overlap(i,2));
            end
            
%             for i= 1:obj.mCount
%                 if maxTimeDiffs(i) ~= 0
%                     samples = A.getSamples();
%                     outputPath = sprintf('/home/houman/Desktop/%i.wav',i);
%                     A.setSamples(samples(round(maxTimeDiffs(i)*A.getFreq()):end));
%                 end
%             end         
        end        
        
        % audios should be of same frequency before this function is called
        function overlaps = getAudiosOverlapAsTimeIndex(obj, sec)
            
            % how many samples at the beginning/end of each Audio should be used for the time synchronization
            numSamplesForAnalysis = sec*obj.mMinFreq;
           
            overlaps = zeros(obj.mCount,2); 
                            
            for i = 1: obj.mCount-1                
                for j = i+1:obj.mCount
                    
                    % two references of Audios
                    A = obj.mAudios(i);
                    B = obj.mAudios(j);
                    
                    beginningSamplesA = A.getSamples(1,numSamplesForAnalysis);
                    beginningSamplesB = B.getSamples(1,numSamplesForAnalysis);
                    [beginningC, beginningLags] = xcorr(beginningSamplesA, beginningSamplesB);         
                                       
                    endSamplesA = A.getSamples(A.getSamplesCount()-numSamplesForAnalysis, A.getSamplesCount());
                    endSamplesB = B.getSamples(B.getSamplesCount()-numSamplesForAnalysis, B.getSamplesCount());
                    [endC, endLags] = xcorr(endSamplesA, endSamplesB);

                    [~,beginningI] = max(abs(beginningC));
                    [~,endI] = max(abs(endC));
                    
                    beginningTimeDiff = beginningLags(beginningI)/obj.mMinFreq;
                    endTimeDiff = endLags(endI)/obj.mMinFreq;
                    
                    if beginningTimeDiff < 0
                        if abs(beginningTimeDiff) > overlaps(i,1)
                            overlaps(j,1) = abs(beginningTimeDiff);
                        end
                    end
                    if beginningTimeDiff > 0
                        if beginningTimeDiff > overlaps(j,1)
                            overlaps(i,1) = beginningTimeDiff;
                        end
                    end
                    
                    if endTimeDiff < 0
                        if abs(endTimeDiff) > overlaps(i,2)
                            duration = B.getSamplesCount()/B.getFreq();
                            overlaps(j,2) = duration-abs(endTimeDiff);
                        end
                    end
                    if endTimeDiff > 0
                        if endTimeDiff > overlaps(j,2)
                            duration = A.getSamplesCount()/A.getFreq();
                            overlaps(i,2) = duration-endTimeDiff;
                        end
                    end
                end
            end
                        
            for i= 1:obj.mCount
                fprintf('Audio %i is %0.3f/%0.3f samples ahead or behined at the beginning/end.\n', i, overlaps(i,1), overlaps(i,2));
            end
            
%             for i= 1:obj.mCount
%                 if maxTimeDiffs(i) ~= 0
%                     samples = A.getSamples();
%                     outputPath = sprintf('/home/houman/Desktop/%i.wav',i);
%                     A.setSamples(samples(round(maxTimeDiffs(i)*A.getFreq()):end));
%                 end
%             end         
        end
        
        
        function minFreq = getMinFreq(obj)
            minFreq = obj.mMinFreq;
        end
             
        function maxFreq = getMaxFreq(obj)
            maxFreq = obj.mMinFreq;
        end        
    end
    methods (Static)
        
        function minFreq = calculateMinFreq(audios)
            minFreq = audios(1).getFreq();
            for i=1:numel(audios)
                if audios(i).getFreq() < minFreq
                    minFreq = audios(i).getFreq();
                end
            end
        end
        
        function maxFreq = calculateMaxFreq(audios)
            maxFreq = audios(1).getFreq();
            for i=1:numel(audios)
                if audios(i).getFreq() < maxFreq
                    maxFreq = audios(i).getFreq();
                end
            end
        end
    end   
end

